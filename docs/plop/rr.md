R package checker
=================

# Introduction
---

R package checker is a tool to check if a new version of an R package (you might be developing) breaks any package which depends on it.

It works with a Bash script and an optional simple web interface. It runs R CMD CHECK on every package that depends on the package to test.

The execution of the R CMD CHECK can be parallelized by giving the corresponding option to the Bash script.

XVFB is used to perform graphical actions even without being under a running X server. JQuery is used to present check results.

There are basically two ways to use R package checker :

* Call the Bash script directly, watch the output and read .Rout files **OR** watch the real-time html output including all .Rout outputs
* Deploy the Php files and adapt values in config.php to manage test launch and results viewing in the basic web interface

**This project is developped within the context of [MBB platform](http://mbb.univ-montp2.fr) services.**

## Table of content
---
1. [Screenshots](#screenshots)
2. [Requirements](#requirements)
3. [Usage](#usage)
    1. [Direct Bash call](#direct-bash-call)
        * without --html
        * with --html
    2. [With the basic web interface](#with-the-basic-web-interface)

# Screenshots
---
**Bash script output :**

![screenshot bash](http://gitlab.mbb.univ-montp2.fr/julien/r-package-checker/raw/master/screenshots/rcheck_bash.png)

---

**Simple web interface example with 3 parallel processes :**

![screenshot web](http://gitlab.mbb.univ-montp2.fr/julien/r-package-checker/raw/master/screenshots/rcheck_web.png)

---

**Something went wrong in plotKML check procedure :**

![screenshot web problem](http://gitlab.mbb.univ-montp2.fr/julien/r-package-checker/raw/master/screenshots/rcheck_web_problem.png)

# Requirements
---
* MANDATORY any version of R (results will be different from one version to another as R CMD CHECK results differ depending on R version...)
* MANDATORY Xvfb
* OPTIONAL Php enabled web server
* OPTIONAL Java is probably needed by one of the packages depending on the package to check
* OPTIONAL Many libraries are probably needed to install dependencies of package to test. If an INSTALL or a CHECK fails, it is quite easy to determine if it is due to a missing lib and which lib it is.
* OPTIONAL configured mail server to be able to send mails (mail command is used by the bash script)

# Usage
---

Basically you can either check a specific package OR just give a list of packages to run R CMD CHECK on them.

The "-c" Bash script option allows you to directly give a list of packages to check. This option is not compatible with giving a package to test ("-p" option).

## Direct Bash call

This is the simple way to use the R package checker. The script prints current steps in stdout and all R output files in the output directory (-d|--dest DESTINATION_DIRECTORY). 

This tool was initially designed to take a package name as the main parameter and check everything that depends on it. This can be done with the -p parameter.

You can also give a list of packages to test (--checklist to give a list with versions and --checklistraw to give a simple list, last versions available are processed).

Another way to use R-package-checker is to pass a folder which contains packages tarballs with -t option. These packages are installed and checked.

Here are the script options :

```
rtest.sh [-p|--package NAME_OF_PACKAGE_TO_TEST || -c|--checklist CHECKLIST_FILE || --checklistraw -t|--tarfolder TAR_FOLDER_PATH] [-l|--libuser R_USER_LIB_VALUE] [-d|--dest DESTINATION_DIRECTORY] [-m|--mail MAIL_TO] [-u|--urlresult URL] [-r|--repo PACKAGE_TO_TEST_REPO] [-x|--xvfb XVFB-RUN PATH] [-n|--nbcores NBCORES] [-s|--skipinstall] [-h|--html] [--rpath PATH_TO_R_BINARY]

-p | --package NAME_OF_PACKAGE_TO_TEST : name of package to test (case sensible), all packages depending on this package are going to be checked by their R CMD CHECK
-c | --checklist CHECKLIST_FILE : file listing packages you want to check (each line should look like : PKG_NAME,PKG_VERSION,anything)
--checklistraw RAW_CHECKLIST_FILE : file listing packages you want to check (simply one package name per line)
-t | --tarfolder TAR_FOLDER_PATH : path to folder where tarballs of packages can be found. All of these packages are going to be \"R CMD CHECKED\"
-r | --repo PACKAGE_TO_TEST_REPO : repository of package you want to test (DEFAULT : R-CRAN)
-l | --libuser R_LIBS_USER : directory where packages depending on PACKAGE_TO_TEST are going to be installed (DEFAULT : R will choose it)
                             This directory can be used several times for several tests. Using same directory several times will save time.
                             This directory can exist or not. If not, it will be created
-d | --dest DESTINATION_DIRECTORY : result directory where HTML result and test files are produced (DEFAULT : ./check_PKGNAME_DATE)
-m | --mail MAIL_TO : email address for real time user information (DEFAULT : NO MAIL WILL BE SENT)
-u | --urlresult URL : if -m option was set, this http link to test results will be sent instead of a file path (DEFAULT : destination directory absolute path)
-x | --xvfb PATH : Specific path to xvfb-run script (DEFAULT : 'xvfb-run')
-n | --nbcores NBCORES : Number of cores for parallel executions of installations (Ncpu parameter) and number of simultaneous checks (DEFAULT : 1)
-s | --skipinstall : flag to skip the packages update/installation
-h | --html : generate html report in DESTINATION_DIRECTORY/index.html . Will download jquery in destination directory
```

### without --html

The script will not produce any fancy output. You'll have to find and read files to see the results of a check.

If there is a failure, check in the output/destination directory to find the corresponding .Rout file.

### with --html

Using the --html option, each step triggers the update of an html page located in the destination/output directory. This page contains the same information than the simple web interface but is static html which does not need Php and a webserver.

## With the basic web interface

The basic web interface launches rtest.sh with -p option to test all packages depending on a specific one.

To setup and use the web interface you need to :

* Copy view.php, start.php, config.php in a directory inside your web root
* Adjust variables in config.php :

```php
// name of the package to test
$package_to_test = "mypkg";

// path to rtest bash script
$rtest_location = "/home/user/rcheck/rtest.sh";

// where you want to install packages
$rlib_location = "/home/user/Rlib_test";

//$email_addr = "Emmanuel.Paradis@univ-montp2.fr";
$email_addr = "your@email.address";

// path to xvfb-run
$xvfbrun_location = "/home/user/xvfb-run";

// directory for all results directories
// (where to put check results)
$all_results_dir = "/home/user/rcheck_results/";

// repository to use to get last version of the package to test
$package_to_test_repo = "http://my.pkg.repo.org/";

// number of threads used for parallel checking
$nb_threads = 4;

// base URL where you host the web interface
// (will prefix start.php and view.php in links)
$base_url = "http://my.server.org/r-package-checker/";
```

* Visit http://your.server.org/rcheck/start.php to launch a check.
* Follow the link given by start.php to view.php to see the real time check progress.
