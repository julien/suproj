
usper proj
=================

# Introduction
---

R package checker is a tool to check if a new version of an R package (you might be developing) breaks any package which depends on it.

It works with a Bash script and an optional simple web interface. It runs R CMD CHECK on every package that depends on the package to test.

The execution of the R CMD CHECK can be parallelized by giving the corresponding option to the Bash script.

XVFB is used to perform graphical actions even without being under a running X server. JQuery is used to present check results.

There are basically two ways to use R package checker :

* Call the Bash script directly, watch the output and read .Rout files **OR** watch the real-time html output including all .Rout outputs
* Deploy the Php files and adapt values in config.php to manage test launch and results viewing in the basic web interface

**This project is developped within the context of [MBB platform](http://mbb.univ-montp2.fr) services.**


# Requirements
---
blabla

# Usage
---

Basically you can either check a specific package OR just give a list of packages to run R CMD CHECK on them.

The "-c" Bash script option allows you to directly give a list of packages to check. This option is not compatible with giving a package to test ("-p" option).

